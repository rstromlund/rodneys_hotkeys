<?php
class Rodneys_Hotkeys extends Plugin {

	private $host;

	function about() {
		return array(1.0,
			"Rodneys_Hotkeys (change view, sort, (un)collapse)",
			"rodneys_mission");
	}

	function init($host) {
		$this->host = $host;
		$host->add_hook($host::HOOK_HOTKEY_INFO, $this);
		$host->add_hook($host::HOOK_HOTKEY_MAP, $this);
	}

	function hook_hotkey_info($hotkeys) {
		$hotkeys[__("Feed")]["rh_collapse"] = __("Un/collapse current (or parent) category");

		$localeView = __("Show articles") . ": ";
		$localeSort = __("Sort articles") . ": ";

		$hotkeys[__("Rodney's Hotkeys")] = array(
			"rh_view_adaptive"		=> $localeView . __("Adaptive"),
			"rh_view_all_articles"	=> $localeView . __("All Articles"),
			"rh_view_starred"			=> $localeView . __("Starred"),
			"rh_view_published"		=> $localeView . __("Published"),
			"rh_view_unread"			=> $localeView . __("Unread"),
			"rh_view_with_note"		=> $localeView . __("With Note"),
			"rh_sort_default"			=> $localeSort . __("Default"),
			"rh_sort_newest"			=> $localeSort . __("Newest first"),
			"rh_sort_oldest"			=> $localeSort . __("Oldest first"),
			"rh_sort_title"			=> $localeSort . __("Title"),
			"rh_logout"					=> __("Logout")
		);
		return $hotkeys;
	}

	function hook_hotkey_map($hotkeys) {
		$hotkeys["x"]   = "rh_collapse";

		$hotkeys["R A"] = "rh_view_adaptive";
		$hotkeys["R L"] = "rh_view_all_articles";
		$hotkeys["R S"] = "rh_view_starred";
		$hotkeys["R P"] = "rh_view_published";
		$hotkeys["R U"] = "rh_view_unread";
		$hotkeys["R W"] = "rh_view_with_note";

		$hotkeys["R D"] = "rh_sort_default";
		$hotkeys["R N"] = "rh_sort_newest";
		$hotkeys["R O"] = "rh_sort_oldest";
		$hotkeys["R T"] = "rh_sort_title";

		$hotkeys["R Q"] = "rh_logout";
		return $hotkeys;
	}

	function get_js() {
		return file_get_contents(__DIR__ . "/init.js");
	}

	function api_version() {
		return 2;
	}

}
