# TTRSS Rodney's Hotkeys Plugin

Add hotkeys to change "Show articles" and "Sort articles" settings. Add a hotkey to replace 'x' un/collapse to include parent category if current selected folder is a feed.

## Prefs

Sometimes I like to read starred articles oldest to newest (especially articles in a series or videos that build on the next/previous one).  I would like to have the "Show articles" and "Sort articles" menu options as hotkeys.  Also I find myself hitting "x" a lot to collapse a category and it doesn't work b/c the current folder is a feed.  This plugin addresses all of these preferences w/o resorting to mouse clicks.

Hotkeys
   * x => Un/collapse current category (or parent category if current selection is a feed)

   * Show articles
     * R A => Adaptive
     * R L => All Articles
     * R S => Starred
     * R P => Published
     * R U => Unread
     * R W => With Note

   * Sort articles
     * R D => Default
     * R N => Newest first
     * R O => Oldest first
     * R T => Title

Thanks to Gilles Grandou's plugin [toggle_sort_titles](https://git.grandou.net/ttrss/toggle_sort_titles) for the pattern/starting point.
