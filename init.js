require(['dojo/_base/kernel', 'dojo/ready', 'fox/Feeds'], function (dojo, ready) {
	ready(function () {
		PluginHost.register(PluginHost.HOOK_INIT_COMPLETE, () => {
			App.hotkey_actions["rh_collapse"]				= rhCollapse;

			App.hotkey_actions["rh_view_adaptive"]			= function () { rhModView('adaptive'); };
			App.hotkey_actions["rh_view_all_articles"]	= function () { rhModView('all_articles'); };
			App.hotkey_actions["rh_view_starred"]			= function () { rhModView('marked'); };
			App.hotkey_actions["rh_view_published"]		= function () { rhModView('published'); };
			App.hotkey_actions["rh_view_unread"]			= function () { rhModView('unread'); };
			App.hotkey_actions["rh_view_with_note"]		= function () { rhModView('has_note'); };

			App.hotkey_actions["rh_sort_default"]			= function () { rhModSort('default'); };
			App.hotkey_actions["rh_sort_newest"]			= function () { rhModSort('feed_dates'); };
			App.hotkey_actions["rh_sort_oldest"]			= function () { rhModSort('date_reverse'); };
			App.hotkey_actions["rh_sort_title"]				= function () { rhModSort('title'); };

			App.hotkey_actions["rh_logout"]					= rhLogout;
		});
	});
});


function rhModView(newVal) {
	console.log('rhModView: ' + newVal);
	const toolbar = document.forms["toolbar-main"];
	const view_mode = dijit.getEnclosingWidget(toolbar.view_mode);

	let value = view_mode.attr('value');
	if (newVal != value) {
		view_mode.attr('value', newVal);
		Feeds.reloadCurrent();
	}
}


function rhModSort(newVal) {
	console.log('rhModSort: ' + newVal);
	const toolbar = document.forms["toolbar-main"];
	const order_by = dijit.getEnclosingWidget(toolbar.order_by);

	let value = order_by.attr('value');
	if (newVal != value) {
		order_by.attr('value', newVal);
		Feeds.reloadCurrent();
	}
}


function rhCollapse() {
	console.log('rhCollapse');

	if (Feeds.activeIsCat()) {
		dijit.byId("feedTree").collapseCat(Feeds.getActive());
	} else {
		dijit.byId("feedTree").collapseCat(Feeds.getCategory(Feeds.getActive()));
	}
}


function rhLogout() {
	console.log('rhLogout');
	App.postCurrentWindow("public.php", {op: "logout", csrf_token: __csrf_token});
}
